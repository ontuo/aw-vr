#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <ctype.h>

//#define DEBAG             //on/off logs

// gcc -l rt test.c
// sudo ./a.out :010321020002D7 /dev/ttyUSB0
// 1st param - ASCII command 
// 2nd param - port

// 0 Modbus ASCII, <7,N,2>
// 09,00 - 1
// 09,01 - 1
// 09,02 - 0
// 09,03 - 0,0
// 09,04 - 0

unsigned char digchar(unsigned char v){
    v-='0';
    if(v>41) return v-39; /* a .. f */
    if(v>9) return v-7; /* A .. F */
    return v; /* 0 .. 9 */
}

unsigned char LRC(unsigned char *str){
    unsigned char val=0;
    while(*str) {
        val+=(digchar(*str)<<4)|digchar(str[1]);
        str+=2;
    }
    return (unsigned char)(-((signed char)val));
}

int main(int argc, char* argv[]) {

    char buffer[50];
    char buffer_1[50];
    struct termios serial;

    #ifdef DEBAG
    printf("Opening %s, command: %s \r\n", argv[2], argv[1]);
    #endif

    int fd = open(argv[2], O_RDWR); 
    if (fd == -1) {
        char buf[20];
        sprintf(buf, "error -> %s", argv[2]);
        perror(buf);
        return -1;
    }

    if (tcgetattr(fd, &serial) < 0) {
        perror("error -> Getting configuration");
        return -1;
    }

    // Set up Serial Configuration
    serial.c_iflag = 0;
    serial.c_oflag = 0;
    serial.c_lflag = 0;
    serial.c_cflag = 0;

    serial.c_cc[VMIN] = 40;
    serial.c_cc[VTIME] = 2;

    // программа блокируется да получения VMIN символов
    // или до истечения VTIME (измеряется в десятой части секунды)
    // а считается это время с момента получения первого символа
    // Т.Е. если нихуя не придет прога зависнет
    // https://blog.mbedded.ninja/programming/operating-systems/linux/linux-serial-ports-using-c-cpp/

    serial.c_cflag = B9600 | CS7 | CREAD | CSTOPB;

    // Apply configuration
    tcsetattr(fd, TCSANOW, &serial); 

    int i = 0;
    for (; i < strlen(argv[1]); i++){
        buffer[i+1] = argv[1][i];
        buffer_1[i] = argv[1][i];
    }
    int lrc_sum = LRC(buffer_1);
    char hex[2];
    sprintf(hex, "%x", lrc_sum);
    buffer[0] = ':';
    buffer[i+1] = toupper(hex[0]);
    buffer[i+2] = hex[1];
    buffer[i+3] = '\r';
    buffer[i+4] = '\n';
    buffer[i+5] = '\0';
    //printf("%d - ", strlen(buffer));
    printf("%s\n", buffer);

    // Attempt to send and receive
    int wcount = write(fd, buffer , strlen(buffer));
    if (wcount < 0) {        
        perror("error -> In function Write Byte");
        return -1;
    }

    #ifdef DEBAG
    printf("Sending -> %s \r\n", argv[1]);
    #endif

    int rcount = read(fd, buffer, sizeof(buffer));
    if (rcount < 0) {
        perror("error -> In function Read Byte");
        return -1;
    }

    #ifdef DEBAG
    printf("Received: %s \r\n", buffer);
    #endif

    close(fd);

    printf("%s", buffer);

}