#!usr/bin/env python3
# :010620000002D7 - turn on
# :010620000001D8 - turn off
#


import sys
import subprocess

def create_cmd(text_cmd, frame):
    if text_cmd == "start":
        frame[4] = 2
        frame[11] = 2
    if text_cmd == "stop":
        frame[4] = 2
        frame[11] = 1
    if text_cmd.find("freq") > 0:
        x = text_cmd.split("-")
        
    if text_cmd == "q":
        sys.exit()

def main():
    frame = ['0','1','0','6','0','0','0','0','0','0','0','0']
    while(1):
        text_cmd = input("Command: ")
        create_cmd(text_cmd, frame)
        write_cmd = "".join(str(x) for x in frame)
        subprocess.call(["./a.out", write_cmd, "/dev/ttyUSB0"])


if __name__ == "__main__":
    main ()
    